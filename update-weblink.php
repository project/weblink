<?php
/**
 * Copy this file to your drupal installation root and run from a web browser
 *
 * BACK UP YOUR DATABASE FIRST!
 */


include_once 'includes/bootstrap.inc';
include_once 'includes/common.inc';


if (module_exist('weblink')) {
  $result = db_queryd("SELECT * FROM {weblink}");
  while ($weblink = db_fetch_object($result)) {
    if (!db_fetch_object(db_query("SELECT * FROM {weblinks_node} WHERE nid = %d", $weblink->nid))) { //only upgrade wen the nodes is not yet in weblinks_node table
      $lid = db_next_id('weblinks_lid');
      db_query("INSERT INTO {weblinks_node} (lid, nid) VALUES ('%d', '%d')", $lid, $weblink->nid);
      db_query("INSERT INTO {weblinks} (lid, url, url_md5, clicks) VALUES (%d, '%s', '%s', %d)", $lid, $weblink->weblink, md5($weblink->weblink),$weblink->click);
      print("Updated weblink '$weblink->weblink'<br/>\n");
    }
  }
}

?>