Description:
------------

This module enables a new artcle type with an associated link. Link clicks
are tracked.

There is a weblink filter that allows you to include urls
in your posts using the following syntax [weblink:<node id>]
or [weblink:<url>]

This module also integrates well with bookmarks.module
providing direct bookmarking capabilities via the a node link.